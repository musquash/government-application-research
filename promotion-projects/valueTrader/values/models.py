from django.db import models
from django.conf import settings
from django.utils import timezone

class Identity(models.Model):
    user = models.ForeignKey(settings.AUTH_USER_MODEL, on_delete=models.CASCADE)
    name = models.CharField(max_length=255)
    email = models.EmailField()
    title = models.CharField(max_length=200)
    text = models.TextField()
    created_date = models.DateTimeField(default=timezone.now)
    wallet0 = models.CharField(max_length=255)
    wallet1 = models.CharField(max_length=255)
    wallet2 = models.CharField(max_length=255)
    wallet3 = models.CharField(max_length=255)

    def create_id(self):
        self.save()

    def __str__(self):
        return self.name
