from django.db import models
from datetime import datetime
from django.conf import settings
from django.utils import timezone

from django.contrib.auth.models import User
from django.db.models.signals import post_save
from django.dispatch import receiver


# Create your models here.

class Wallets(models.Model):
    owner = models.ForeignKey(User, related_name='owner', on_delete=models.CASCADE)
    privateKey = models.CharField(max_length=255)
    address = models.CharField(max_length=255)
    type = models.CharField(max_length=255)
    symbol  = models.CharField(max_length=255)
    balance = models.DecimalField(max_digits=19, decimal_places=3)
    #trade_blc = models.DecimalField(max_digits=19, decimal_places=18)

    class Meta:
        # Gives the proper plural name for admin
        verbose_name_plural = "Wallets"

    def __str__(self):
        return str(self.owner)


class Contracts(models.Model):
    address = models.CharField(max_length=255)

    class Meta:
        # Gives the proper plural name for admin
        verbose_name_plural = "Contracts"

    def __str__(self):
        return str(self.address)
