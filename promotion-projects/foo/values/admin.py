from django.contrib import admin
from .models import Wallets, Contracts

# Register your models here.

admin.site.register(Wallets)
admin.site.register(Contracts)
