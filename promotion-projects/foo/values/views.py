from django.shortcuts import render, redirect, get_object_or_404
from django.http import HttpResponse
from .models import Wallets, Contracts
from django.contrib.auth.models import User
from django.contrib.auth.forms import  AuthenticationForm
from django.contrib.auth import login, logout, authenticate
from django.contrib import messages
from .forms import NewUserForm
from values import blockchain
import pandas as pd

# Create your views here.

def web3Data(request):
    b = blockchain.Blockchain()
    print(b.w3.isConnected())
    status = b.w3.isConnected()
    hashrate = b.w3.eth.hashrate
    contract_address = b.w3.toChecksumAddress('0xd86fa55e3e5bb3a2aa56ae40c4167fbd12be26d8')
    blcn = b.get_balance('0xe4CC5920f23E8d8726F48d05b8262be66a04FABd')
    blcn = b.w3.fromWei(blcn, 'ether')
    token_information = b.get_information_of_token(contract_address)
    balance_musquash = b.get_token_balance(b.musquash, token_information)
    wallets = Wallets.objects.all()
    contracts = Contracts.objects.all()
    users = User.objects.all()
    contract_addresses = []
    token_infos = pd.DataFrame(columns=['name', 'symbol', 'totalSupply', 'address'])
    for contract in contracts:
        contract_addresses.append(b.get_information_of_token(str(contract)))
        token = b.get_information_of_token(str(contract))
        token_infos[len(token_infos)] = [token[0], token[1], token.totalSupply, token.address]

    print(request.user.username)
    print(f"token Infos are {token_infos['name']}")
    # print(f"The adresses of all contracts are {contract_addresses}")




    # for wallet in wallets:
    #     if wallet.owner.username == request.user.username:
    #         for token in contract_addresses:
    #             print(contract)
    #             token_info_list.append(b.get_token_balance(wallet.address, token.address))
    # print(token_info_list)

    return render(request, 'values/home.html',
        {
            'status':status,
            #'wallets':wallets,
            #'wallets_values':wallets_values,
            'hashrate':hashrate,
            #'accounts':accounts,
            'balance':blcn,
            # 'contract':contract_address,
            #'contract_functions':contract_functions
            'token_infos': token_information,
            'token_name': token_information[0],
            'token_symbol':token_information[1],
            'wallet_address': b.musquash,
            'balance_musquash':balance_musquash,
            'wallets':wallets,
            'contracts':contracts,
            'contract_addresses':contract_addresses,
            # 'test_wallet': wallet,
        })



def register(request):
    if request.method == 'POST':
        form = NewUserForm(request.POST)
        if form.is_valid():
            user = form.save()
            username = form.cleaned_data.get('username')
            messages.success(request, f"New Account created: {username}")
            login(request, user)
            messages.info(request, f"You are now loged in as user: {username}")
            return redirect('values:homepage')
        else:
            for msg in form.error_messages:
                messages.error(request, f'{msg}: {form.error_messages[msg]}')
                print(form.error_messages[msg])

    form = NewUserForm
    return render(request,
                  "values/register.html",
                  context={"form":form})

def logout_request(request):
    logout(request)
    messages.info(request, "Logged out successfully.")
    return redirect("/")

def login_request(request):
    if request.method == 'POST':
        form = AuthenticationForm(request=request, data=request.POST)
        if form.is_valid():
            username = form.cleaned_data.get('username')
            password = form.cleaned_data.get('password')
            user = authenticate(username=username, password=password)
            if user is not None:
                login(request, user)
                messages.info(request, f"You are now logged in as {username}")
                return redirect('/')
            else:
                messages.error(request, "Invalid username or password.")
        else:
            messages.error(request, "Invalid username or password.")
    form = AuthenticationForm()
    return render(request = request,
                    template_name = "values/login.html",
                    context={"form":form})
