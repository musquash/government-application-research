
from web3 import Web3
from web3.exceptions import ValidationError
# from populus.utils.wait import wait_for_transaction_receipt
from collections import namedtuple
from os.path import exists
import json
import os

SendTransaction = namedtuple("SendTransaction", "sender password destination amount fee")
TokenInformation = namedtuple("TokenInformation", "name symbol totalSupply address")


# # url = 'https://rinkeby.infura.io/v3/c91912c87f3f4641b9f4c506c405e932'
# url = 'https://ropsten.infura.io/v3/06f57317421245459cdbcfbd39c41322'
# w3 = Web3(Web3.HTTPProvider(url))

#abi = open("/Users/Philipp/Git/promotion-projects/Wertesystem/eth_board/contracts/build/contracts/testToken.json","rb")
with open("/Users/Philipp/Git/promotion-projects/Wertesystem/eth_board/contracts/build/contracts/testToken.json") as file:
    tmp = json.load(file)
    abi = tmp['abi'] # The file contains more information than just the abi. We are only interessted in the abi.


path_to_contracts = os.path.abspath('./contracts/')


class Blockchain:

    with open("/Users/Philipp/Git/promotion-projects/Wertesystem/eth_board/contracts/build/contracts/testToken.json") as file:
        tmp = json.load(file)
        abi = tmp['abi'] # The file contains more information than just the abi. We are only interessted in the abi.

    musquash = '0xe4CC5920f23E8d8726F48d05b8262be66a04FABd'
    alice = '0x29ff4f5004bece6bdeb47dd761d1782ee18e21d9'

    tokens_file = abi

    def __init__(self):
        url = 'https://ropsten.infura.io/v3/06f57317421245459cdbcfbd39c41322'
        self.w3 = Web3(Web3.HTTPProvider(url))

    # def get_accounts(self):
    #     return map(lambda account: (account, self.w3.fromWei(self.w3.eth.getBalance(account), 'ether')), self.w3.eth.accounts)

    # def get_default_account(self):
    #     return self.w3.eth.defaultAccount

    def create_new_account(self, password):
        return self.w3.eth.create(password)

    def get_balance(self, address):
        return self.w3.fromWei(self.w3.eth.getBalance(address), 'ether')

    def get_token_balance(self, account_address, token_information):
        try:
            token_contract = self.w3.eth.contract(address=token_information.address, abi=abi)
            balance = token_contract.functions.balanceOf(account_address).call()
        except ValidationError:
            return None
        return balance

    # def create_send_transaction(self, tx):
    #     nonce = self.w3.eth.getTransactionCount(tx.sender)
    #     transaction = {
    #       'from': tx.sender,
    #       'to': Web3.toChecksumAddress(tx.destination),
    #       'value': self.w3.toWei(str(tx.amount), 'ether'),
    #       'gas': 21000,
    #       'gasPrice': self.w3.toWei(str(tx.fee), 'gwei'),
    #       'nonce': nonce
    #     }
    #
    #     tx_hash = self.w3.personal.sendTransaction(transaction, tx.password)
    #     wait_for_transaction_receipt(self.w3, tx_hash)
    #
    # def create_send_token_transaction(self, tx, token_information):
    #     nonce = self.w3.eth.getTransactionCount(tx.sender)
    #     token_contract = self.w3.eth.contract(address=token_information.address, abi=erc20_token_interface)
    #     transaction = token_contract.functions.transfer(tx.destination, int(tx.amount)).buildTransaction({
    #               'from': tx.sender,
    #               'gas': 70000,
    #               'gasPrice': self.w3.toWei(str(tx.fee), 'gwei'),
    #               'nonce': nonce
    #           })
    #
    #     tx_hash = self.w3.personal.sendTransaction(transaction, tx.password)
    #     wait_for_transaction_receipt(self.w3, tx_hash)

    def get_information_of_token(self, address):
        try:
            #erc20_token_interface = tokens_file
            token_contract = self.w3.eth.contract(address=address, abi=abi)
            name = token_contract.functions.name().call()
            symbol = token_contract.functions.symbol().call()
            total_supply = token_contract.functions.totalSupply().call()
        except ValidationError:
            return None
        token_information = TokenInformation(name=name,
                                             symbol=symbol,
                                             totalSupply=total_supply,
                                             address=address)
        return token_information

    def get_token_named_tuple(self, token_dict, address):
        return TokenInformation(name=token_dict['name'],
                                totalSupply=token_dict['total_supply'],
                                symbol=token_dict['symbol'],
                                address=address)

    def get_tokens(self):
        tokens = {}
        if exists(self.tokens_file):
            with open(self.tokens_file) as json_data:
                tokens = json.load(json_data)
        return tokens


blockchain = Blockchain()
