from django.db import models
from datetime import datetime
from tinymce.models import HTMLField
from django.conf import settings
from django.utils import timezone

from django.contrib.auth.models import User
from django.db import models
from django.db.models.signals import post_save
from django.dispatch import receiver

# Create your models here.


class Identity(models.Model):
    user = models.ForeignKey(settings.AUTH_USER_MODEL, on_delete=models.CASCADE)
    name = models.CharField(max_length=255)
    email = models.EmailField()
    created_date = models.DateTimeField(default=timezone.now)

    class Meta:
        # Gives the proper plural name for admin
        verbose_name_plural = "Identities"

    def __str__(self):
        return self.name



# class Account(User):
#     user = models.OneToOneField(User, on_delete=models.CASCADE, parent_link=True)
#     bio = models.TextField(max_length=500, blank=True)
#     location = models.CharField(max_length=30, blank=True)
#     birth_date = models.DateField(null=True, blank=True)
#     wallet = models.CharField(max_length=300)
#
#     @receiver(post_save, sender=User)
#     def create_user_profile(sender, instance, created, **kwargs):
#         if created:
#             Account.objects.create(user=instance)
#
#     @receiver(post_save, sender=User)
#     def save_user_profile(sender, instance, **kwargs):
#         instance.account.save()




class Wallets(models.Model):
    owner = models.ForeignKey(Identity, on_delete=models.CASCADE)
    admin = models.ForeignKey(User, related_name='owner', on_delete=models.CASCADE)
    privateKey = models.CharField(max_length=255)
    wallet_address = models.CharField(max_length=255)
    type = models.CharField(max_length=200)
    symbol  = models.CharField(max_length=255)
    balance = models.DecimalField(max_digits=19, decimal_places=3)
    #trade_blc = models.DecimalField(max_digits=19, decimal_places=18)

    class Meta:
        # Gives the proper plural name for admin
        verbose_name_plural = "Wallets"

    def __str__(self):
        return str(self.owner)


class Accounts(models.Model):
    owner = models.ForeignKey(User, related_name='topics', on_delete=models.CASCADE)
    privateKey = models.CharField(max_length=255)
    wallet_address = models.CharField(max_length=255)
    type = models.CharField(max_length=200)
    symbol  = models.CharField(max_length=255)
    balance = models.DecimalField(max_digits=19, decimal_places=3)







class TutorialCategory(models.Model):

    tutorial_category = models.CharField(max_length=200)
    category_summary = models.CharField(max_length=200)
    category_slug = models.CharField(max_length=200, default=1)

    class Meta:
        # Gives the proper plural name for admin
        verbose_name_plural = "Categories"

    def __str__(self):
        return self.tutorial_category

class TutorialSeries(models.Model):
    tutorial_series = models.CharField(max_length=200)

    tutorial_category = models.ForeignKey(TutorialCategory, default=1,
                                          verbose_name="Category",
                                          on_delete=models.SET_DEFAULT)
    series_summary = models.CharField(max_length=200)

    class Meta:
        # otherwise we get "Tutorial Seriess in admin"
        verbose_name_plural = "Series"

    def __str__(self):
        return self.tutorial_series

class Tutorial(models.Model):
    tutorial_title = models.CharField(max_length=200)
    tutorial_content = models.TextField()
    tutorial_published = models.DateTimeField('date published')
    #https://docs.djangoproject.com/en/2.1/ref/models/fields/#django.db.models.ForeignKey.on_delete
    tutorial_series = models.ForeignKey(TutorialSeries, default=1,
                                        verbose_name="Series",
                                        on_delete=models.SET_DEFAULT)
    tutorial_slug = models.CharField(max_length=200, default=1)
    def __str__(self):
        return self.tutorial_title
