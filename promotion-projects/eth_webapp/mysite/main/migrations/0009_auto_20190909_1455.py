# Generated by Django 2.2.5 on 2019-09-09 12:55

from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):

    dependencies = [
        ('main', '0008_delete_account'),
    ]

    operations = [
        migrations.AddField(
            model_name='wallets',
            name='owner',
            field=models.ForeignKey(default=1, on_delete=django.db.models.deletion.CASCADE, to='main.Identity'),
            preserve_default=False,
        ),
        migrations.AddField(
            model_name='wallets',
            name='privateKey',
            field=models.CharField(default=1, max_length=255),
            preserve_default=False,
        ),
    ]
