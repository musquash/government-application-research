from django.contrib import admin
from .models import Tutorial, TutorialSeries, TutorialCategory,  Identity, Wallets
from tinymce.widgets import TinyMCE
from django.db import models

# Register your models here.

class TutorialAdmin(admin.ModelAdmin):

    fieldsets = [
        ("Title/date", {'fields': ["tutorial_title", "tutorial_published"]}),
        ("URL", {'fields': ["tutorial_slug"]}),
        ("Series", {'fields': ["tutorial_series"]}),
        ("Content", {"fields": ["tutorial_content"]})
    ]

    formfield_overrides = {
        models.TextField: {'widget': TinyMCE(attrs={'cols': 80, 'rows': 30})},
        }

# class WalletsAdmin(admin.ModelAdmin):
#
#     fieldsets = [
#     ("User", {'fields' : ['owner']}),
#     ('Wallet Information', {'fields' : ['privateKey', 'wallet', 'type', 'balance']})
#     ]


admin.site.register(TutorialSeries)
admin.site.register(TutorialCategory)
admin.site.register(Tutorial,TutorialAdmin)
admin.site.register(Identity)
admin.site.register(Wallets)
# admin.site.register(Wallets, WalletsAdmin)
# admin.site.register(Account)
