from django.shortcuts import render, redirect, get_object_or_404
from django.http import HttpResponse
from .models import Tutorial, TutorialCategory, TutorialSeries, Wallets
from django.contrib.auth.models import User
from django.contrib.auth.forms import  AuthenticationForm
from django.contrib.auth import login, logout, authenticate
from django.contrib import messages
from .forms import NewUserForm
from main import blockchain

from web3 import Web3
# url = 'https://rinkeby.infura.io/v3/c91912c87f3f4641b9f4c506c405e932'
url = 'https://ropsten.infura.io/v3/06f57317421245459cdbcfbd39c41322'
web3 = Web3(Web3.HTTPProvider(url))

abi = open("/Users/Philipp/Git/promotion-projects/Wertesystem/eth_board/contracts/build/contracts/testToken.json","r")

b = blockchain.Blockchain()


# Create your views here.

def trade(request):
    #return HttpResponse("Hello, World!")
    #ids = Identity.objects.all()
    #wallets = Wallets.objects.all()
    #wallets_values = wallets.values()
    print(web3.isConnected())
    status = web3.isConnected()
    hashrate = web3.eth.hashrate
    #accounts = web3.eth.accounts
    contract_address = web3.toChecksumAddress('0xd86fa55e3e5bb3a2aa56ae40c4167fbd12be26d8')
    #contract = web3.eth.contract(contract_address)
    blcn = b.get_balance('0xe4CC5920f23E8d8726F48d05b8262be66a04FABd')
    blcn = web3.fromWei(blcn, 'ether')
    token_information = b.get_information_of_token(contract_address)
    print(blcn)
    #contract_functions = contract.functions
    #address = contract.address
    # accounts = list(b.get_accounts())
    #new_account = b.create_new_account('123')
    # default = b.get_default_account()
    print(web3.eth.blockNumber)
    balance_musquash = b.get_token_balance(b.musquash, token_information)
    wallets = Wallets.objects.all()
    # wallets = Wallets.objects.get(Wallets.pk)
    # wallets_specific = get_object_or_404(Wallets)
    # wallet = get_object_or_404(Wallets, User.username)
    return render(request, 'main/eth.html',
        {
            #'ids' : ids,
            'status':status,
            #'wallets':wallets,
            #'wallets_values':wallets_values,
            'hashrate':hashrate,
            #'accounts':accounts,
            'balance':blcn,
            'contract':contract_address,
            #'contract_functions':contract_functions
            'token_infos': token_information,
            'token_name': token_information[0],
            'token_symbol':token_information[1],
            'wallet_address': b.musquash,
            'balance_musquash':balance_musquash,
            'wallets':wallets,
            # 'test_wallet': wallet,
        })



def single_slug(request, single_slug):
    categories = [c.category_slug for c in TutorialCategory.objects.all()]
    if single_slug in categories:
        matching_series = TutorialSeries.objects.filter(tutorial_category__category_slug=single_slug)

        series_urls = {}
        for m in matching_series.all():
            part_one = Tutorial.objects.filter(tutorial_series__tutorial_series=m.tutorial_series).earliest("tutorial_published")
            series_urls[m] = part_one.tutorial_slug

        return render(request,
                      "main/category.html",
                      {"part_ones": series_urls})

    tutorials = [c.tutorial_slug for t in TutorialTutorial.objects.all()]
    if single_slug in tutorials:
        return HttpResponse(f"{single_slug} is a tutorial!!!")

    return HttpResponse(f"{single_slug} does not correspond to anything")

def homepage(request):
    return render(request=request,
                  template_name='main/categories.html',
                  context={"categories": TutorialCategory.objects.all()})


def register(request):
    if request.method == 'POST':
        form = NewUserForm(request.POST)
        if form.is_valid():
            user = form.save()
            username = form.cleaned_data.get('username')
            messages.success(request, f"New Account created: {username}")
            login(request, user)
            messages.info(request, f"You are now loged in as user: {username}")
            return redirect('main:homepage')
        else:
            for msg in form.error_messages:
                messages.error(request, f'{msg}: {form.error_messages[msg]}')
                print(form.error_messages[msg])

    form = NewUserForm
    return render(request,
                  "main/register.html",
                  context={"form":form})

def logout_request(request):
    logout(request)
    messages.info(request, "Logged out successfully.")
    return redirect("/")

def login_request(request):
    if request.method == 'POST':
        form = AuthenticationForm(request=request, data=request.POST)
        if form.is_valid():
            username = form.cleaned_data.get('username')
            password = form.cleaned_data.get('password')
            user = authenticate(username=username, password=password)
            if user is not None:
                login(request, user)
                messages.info(request, f"You are now logged in as {username}")
                return redirect('/')
            else:
                messages.error(request, "Invalid username or password.")
        else:
            messages.error(request, "Invalid username or password.")
    form = AuthenticationForm()
    return render(request = request,
                    template_name = "main/login.html",
                    context={"form":form})
